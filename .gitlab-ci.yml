stages:
  - lint
  - test
  - build
  - deploy
  - review

default:
  image: node:14.17.0-alpine
  cache:
    key:
      files:
        - package-lock.json
    paths:
      - .npm/
  before_script:
    - npm ci --cache .npm --prefer-offline

.setup_ssh:
  before_script:
    - 'which ssh-agent || (apk add --update openssh curl bash git)'
    - eval $(ssh-agent -s)
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" > ~/.ssh/known_hosts

eslint:
  stage: lint
  script:
    - npm run eslint

prettier:
  stage: lint
  script:
    - npm run prettier:check

typecheck:
  stage: lint
  script:
    - npm run typecheck

react-tests:
  stage: test
  script:
    - npm run test

build:
  stage: build
  variables:
    APP_NAME: $CI_COMMIT_REF_SLUG
    REACT_APP_BACKEND_URL: 'http://$APP_NAME.$APP_HOST/api'
  script:
    - npm run build
  artifacts:
    paths:
      - build
    expire_in: 1 day

deploy:
  extends:
    - .setup_ssh
  stage: deploy
  variables:
    DEPLOY_DST: '/home/deploy/demo/$CI_COMMIT_REF_SLUG/public/'
  script:
    - ssh $SSH_USER@$SSH_HOST "mkdir -p $DEPLOY_DST"
    - scp -r build/* $SSH_USER@$SSH_HOST:$DEPLOY_DST
  rules:
    - if: $CI_PIPELINE_SOURCE == 'trigger'
      when: never
  rules:
    - if: $CI_COMMIT_BRANCH == 'main'
      when: manual

start_review:
  stage: review
  extends:
    - .setup_ssh
  variables:
    BRANCH_EXISTS_URL: 'https://gitlab.com/api/v4/projects/$BACKEND_PROJECT_ID/repository/branches/$CI_COMMIT_REF_NAME'
    CREATE_BRANCH_URL: 'https://gitlab.com/api/v4/projects/$BACKEND_PROJECT_ID/repository/branches?branch=$CI_COMMIT_REF_NAME&ref=$CI_DEFAULT_BRANCH'
    TRIGGER_PIPELINE_URL: 'https://gitlab.com/api/v4/projects/$BACKEND_PROJECT_ID/pipeline?ref=$CI_COMMIT_REF_NAME'
  script:
    - 'status_code=$(curl -I --header "PRIVATE-TOKEN: $PAT_TOKEN" --write-out "%{http_code}" --silent --output /dev/null "$BRANCH_EXISTS_URL")'
    - '[[ "$status_code" -ne 204 ]] && status_code=$(curl -X POST --header "PRIVATE-TOKEN: $PAT_TOKEN" --write-out "%{http_code}" --silent --output /dev/null "$CREATE_BRANCH_URL")'
    - '[[ "$status_code" -ne 400 ]] && echo "Branch already exists" && exit 0'
    - 'curl -X POST --header "PRIVATE-TOKEN: $PAT_TOKEN" --write-out "%{http_code}" --silent --output /dev/null "$TRIGGER_PIPELINE_URL"'
    - ssh $SSH_USER@$SSH_HOST "mkdir -p $DEPLOY_DST"
    - scp -r build/* $SSH_USER@$SSH_HOST:$DEPLOY_DST
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: http://$CI_COMMIT_REF_SLUG.$APP_HOST/
    on_stop: stop_review
  rules:
    - if: '$CI_MERGE_REQUEST_TITLE =~ /SKIP REVIEW/'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

stop_review:
  stage: review
  extends:
    - .setup_ssh
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  variables:
    DEPLOY_DST: '/home/deploy/demo/$CI_COMMIT_REF_SLUG/public'
  script:
    - ssh $SSH_USER@$SSH_HOST "rm -rf $DEPLOY_DST"
  rules:
    - if: '$CI_MERGE_REQUEST_TITLE =~ /SKIP REVIEW/'
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual

triggered_review:
  extends:
    - .setup_ssh
  stage: review
  variables:
    DEPLOY_DST: '/home/deploy/demo/$APP_NAME/public'
  script:
    - ssh $SSH_USER@$SSH_HOST "mkdir -p $DEPLOY_DST"
    - scp -r build/* $SSH_USER@$SSH_HOST:$DEPLOY_DST
  environment:
    name: review/$APP_NAME
    url: http://$APP_NAME.$APP_HOST/
    on_stop: triggered_stop_review
  rules:
    - if: $CI_PIPELINE_SOURCE == 'trigger'

triggered_stop_review:
  stage: review
  extends:
    - .setup_ssh
  environment:
    name: review/$APP_NAME
    action: stop
  script:
    - ssh $SSH_USER@$SSH_HOST "rm -rf $DEPLOY_DST/public"
  rules:
    - if: $CI_PIPELINE_SOURCE == 'trigger'
      when: manual
