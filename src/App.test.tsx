import React from 'react'
import { render } from '@testing-library/react'
import App from './App'

test('renders learn app logo', () => {
  const { container } = render(<App />)
  const imgEl = container.getElementsByClassName('App-logo')[0]
  expect(imgEl).toBeInTheDocument()
})
