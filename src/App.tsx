import React, { useCallback, useEffect, useState } from 'react'
import logo from './logo.svg'
import './App.css'

const API_URL =
  process.env.REACT_APP_BACKEND_URL || 'http://main.18.198.207.183.nip.io/api'

const App: React.FC = () => {
  const [message, setMessage] = useState<string>()
  const [error, setError] = useState<Error | null>()
  const [isLoading, setIsloading] = useState(false)

  const getMessageFromServer = useCallback(async () => {
    try {
      setError(null)
      setIsloading(true)

      const res = await fetch(API_URL)

      setMessage(await res.text())
    } catch (error) {
      setError(error as Error)
    } finally {
      setIsloading(false)
    }
  }, [])

  useEffect(() => {
    getMessageFromServer()
  }, [])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {!error && (
          <p>Message from server: {isLoading ? 'Loading...' : message}</p>
        )}
        {error && <p>Error: {error.message}</p>}
      </header>
    </div>
  )
}

export default App
